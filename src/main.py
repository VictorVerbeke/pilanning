import argparse
from PIL import Image, ImageDraw, ImageFont, ImageOps, ImageFilter
import toml
import os
import logging
import math
import sys

import datetime

# Useful consts
days_of_the_week = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]
months = {
    1: "Janvier", 2: "Février", 3: "Mars", 4: "Avril", 5: "Mai", 6: "Juin",
    7: "Juillet", 8: "Août", 9: "Septembre", 10: "Octobre", 11: "Novembre", 12: "Décembre"
}
theme_colors = {
    "grey": (0xca, 0xca, 0xca),
    "cherry":{
        "light": (0xff, 0xe7, 0xff),
        "strong":(0x54, 0xca, 0x86),
    }, 
    "winter":{
        "light": (0xe5, 0xf6, 0xff),
        "strong":(0x3f, 0x99, 0xec),
    },
    "pond":{
        "light": (0xe5, 0xff, 0xe6),
        "strong":(0xd6, 0x48, 0x59),
    },
    "bridge":{
        "light": (0xff, 0xfb, 0xec),
        "strong":(0xde, 0xc1, 0x4d),
    },
    "stars":{
        "light": (0xcc, 0xf6, 0xfa),
        "strong":(0xcc, 0xf6, 0xfa),
    }
}

ultra_default_config = {
    "logo_color": "white",
    "background_logo_color": "black",
    "portrait_logo_slot": "bottom",
    "portrait_x_offset": 0,
    "portrait_y_offset": 0,
    "landscape_logo_slot": "right",
    "landscape_x_offset": 0,
    "landscape_y_offset": 0,
    "daily_x_offset": 0,
    "daily_y_offset": 0,
}

# Color things
class CustomFormatter(logging.Formatter):

    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = "%(asctime)s - %(levelname)7s (%(filename)s:%(lineno)4d) - %(message)s"

    FORMATS = {
        logging.DEBUG: grey + format + reset,
        logging.INFO: grey + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

# Tool functions
def check_if_else_default(dict, key, default):
    if key not in dict:
        return default
    return dict[key]

def arguments():
    # Execute parsing
    parser = argparse.ArgumentParser(
            prog= "PILanning",
            description = "Create a planning using PIL.",
            epilog = "Made by Misou.")
    parser.add_argument("filename")
    return parser.parse_args()

def get_config(config_path):
    """Check if the config is valid"""
    with open(config_path, "r") as f:
            lines = f.read()
    config = toml.loads(lines)

    error_flag = False
    for key, value in config.items():
        if key not in ["layout", "week"] + days_of_the_week:
            logging.error(f"Key {key} is invalid in the given config in path {config_path}.")

    for possible_key in days_of_the_week:
        if possible_key in config:
            for key, value in config[possible_key].items():
                if key not in [
                    "type",
                    "game",
                    "landscape_logo_slot",
                    "portrait_logo_slot",
                    "background_logo_color",
                    "logo_color",
                    "portrait_x_offset",
                    "portrait_y_offset",
                    "landscape_x_offset",
                    "landscape_y_offset",
                    "daily_x_offset",
                    "daily_y_offset",
                    
                    ]:
                    logging.error(f"Key {key} is not valid for a day of the week")
                    error_flag = True
                if key == "type" and value not in ["20h", "14h"]:
                    logging.error(f"Key {key} does not describe a good hour (14h or 20h).")
                    error_flag = True
                if key == "logo_slot" and value not in ["top", "middle", "bottom"]:
                    logging.error(f"Key {key} does not describe a good position (top, bottom, middle).")
                if key == "background_logo_color" and value not in ["black", "white"]:
                    logging.error(f"Key {key} does not describe a good position (black, white).")

    if "layout" not in config:
        logging.warning(f"Layout not specified in config.")
    if "week" not in config:
        logging.error(f"Week not specified. Please refer to week number.")
        error_flag = True
    if error_flag:
        logging.critical("Error during Config parsing. Exiting.")
        exit(1)
    return config

def draw_text_in_font(draw_target, coords, border=0, content=[]):
    fonts = {}
    for text, color, font_name, font_size in content:
        font = fonts.setdefault(font_name, ImageFont.truetype(font_name, font_size))
        (x, y) = coords
        draw_target.text(coords, text, "black", font, stroke_width = border, stroke_color = "black")
        draw_target.text(coords, text, color, font)
        coords = (coords[0] + font.getlength(text), coords[1])

def remove_transparent_pixels(image):
    r,g,b,a = image.split()
    a = a.point(lambda i: 255 if i == 255 else 0)
    image = Image.merge("RGBA", (r,g,b,a))
    return image

def format_to_four_unpack(image):
    if len(image.split()) == 2:
        l,a = image.split()
        image = Image.merge("RGBA", (l, l, l, a))
    elif len(image.split()) == 3:
        l, _, a =  image.split()
        image = Image.merge("RGBA", (l, l, l, a))
    return image

# Generation functions
def generate_planning_landscape(config):
    """Config : layout, week, days [...]"""

    def set_planning_title_text(image):
        # Setup the text
        font_size = 60
        text_image = Image.new("RGBA", (2000, 1000), (0,0,0,0))
        draw = ImageDraw.Draw(text_image)
        draw.ink = "black"
        draw.fill = True
        if last_day > first_day:
            color_scheme = theme_colors[config["layout"]]
            draw_text_in_font(draw, (100, 100), border = 2, content = [
                ("Planning du ", color_scheme["light"], os.path.join(fonts_dir, "Rodin Pro L.otf"), font_size),
                (f"{first_day} ", color_scheme["strong"], os.path.join(fonts_dir, "Rodin Pro UB.otf"), font_size),
                ("au ", color_scheme["light"], os.path.join(fonts_dir, "Rodin Pro L.otf"), font_size),
                (f"{last_day} ", color_scheme["strong"], os.path.join(fonts_dir, "Rodin Pro UB.otf"), font_size),
                (f"{months[first_date.month]}", color_scheme["strong"], os.path.join(fonts_dir, "Rodin Pro DB.otf"), font_size)
            ])
        else:
            color_scheme = theme_colors[config["layout"]]
            draw_text_in_font(draw, (100, 100), border = 2, content = [
                ("Planning du ", color_scheme["light"], os.path.join(fonts_dir, "Rodin Pro L.otf"), font_size),
                (f"{first_day} ", color_scheme["strong"], os.path.join(fonts_dir, "Rodin Pro UB.otf"), font_size),
                (f"{months[first_date.month]} ", color_scheme["strong"], os.path.join(fonts_dir, "Rodin Pro DB.otf"), font_size), # Additional line 
                ("au ", color_scheme["light"], os.path.join(fonts_dir, "Rodin Pro L.otf"), font_size),
                (f"{last_day} ", color_scheme["strong"], os.path.join(fonts_dir, "Rodin Pro UB.otf"), font_size),
                (f"{months[last_date.month]}", color_scheme["strong"], os.path.join(fonts_dir, "Rodin Pro DB.otf"), font_size),
            ])

        # Crop image 
        bbox = text_image.getbbox()
        text_image = text_image.crop(bbox)
        (w, _) = text_image.size
        image.paste(text_image, (int(landscape_width/2 - w/2), 60), text_image)

    def add_game_tile(image, day, day_config):
        # Extract values from day_config
        # Try to extract default values
        time_slot = check_if_else_default(day_config, "type", None)
        game_name = check_if_else_default(day_config, "game", None)
        game_config = ultra_default_config
        if os.path.exists(os.path.join(games_dir, game_name, "settings.toml")):
            with open(os.path.join(games_dir, game_name, "settings.toml"), "r") as f:
                loaded_config = toml.loads(f.read())
            for key, value in loaded_config.items():
                game_config[key] = value

        logo_color = check_if_else_default(day_config, "logo_color", game_config["logo_color"])
        background_logo_color = check_if_else_default(day_config, "background_logo_color", game_config["background_logo_color"])
        landscape_logo_slot = check_if_else_default(day_config, "landscape_logo_slot", game_config["landscape_logo_slot"])
        landscape_x_offset = check_if_else_default(day_config, "landscape_x_offset", game_config["landscape_x_offset"])
        landscape_y_offset = check_if_else_default(day_config, "landscape_y_offset", game_config["landscape_y_offset"])
        
        # Compute coords of the time
        monday_offset = 114
        two_days_diff = 250
        offset_20h = 667
        offset_14h = 240
        horizontal_coord = monday_offset + days_of_the_week.index(day) * two_days_diff
        vertical_coord = offset_20h if time_slot == "20h" else offset_14h
        tile_coords = (horizontal_coord, vertical_coord)

        # Retrieve all data from the game folder
        artwork_image = None
        logo_image = None
        for extension in ["jpeg", "jpg", "png"]:
            if os.path.exists(os.path.join(games_dir, game_name, f"artwork.{extension}")):
                artwork_image = Image.open(os.path.join(games_dir, game_name, f"artwork.{extension}"))
                break
        if artwork_image is None:
            logging.error(f"Artwork image for game {game_name} is not found.")
            return

        for extension in ["png"]:
            if os.path.exists(os.path.join(games_dir, game_name, f"logo.{extension}")):
                logo_image = Image.open(os.path.join(games_dir, game_name, f"logo.{extension}"))
                logo_image = format_to_four_unpack(logo_image)
                break
        if logo_image is None:
            logging.warning(f"Logo image for game {game_name} is not found. Might be intentional.")

        if time_slot == "20h":
            tile_alpha_image = Image.open(os.path.join(base_template_dir, "20h_alpha_tile.png"))
            tile_contour_image = Image.open(os.path.join(base_template_dir, "20h_tile.png"))
        else:
            tile_alpha_image = Image.open(os.path.join(base_template_dir, "14h_alpha_tile.png"))
            tile_contour_image = Image.open(os.path.join(base_template_dir, "14h_tile.png"))

        # First, paste the tile_alpha
        image.paste(tile_alpha_image, tile_coords, tile_alpha_image)

        # Multiply the artwork with the alpha
        artwork_image_ratio = (artwork_image.size[0] / artwork_image.size[1])
        tile_alpha_image_ratio = (tile_alpha_image.size[0] / tile_alpha_image.size[1])
        if artwork_image_ratio > tile_alpha_image_ratio :
            x_scale = int(tile_alpha_image.size[1] * artwork_image_ratio)
            y_scale = int(tile_alpha_image.size[1])
            artwork_image = artwork_image.resize((x_scale, y_scale), Image.LANCZOS)
        else:
            x_scale = int(tile_alpha_image.size[0] * artwork_image_ratio)
            y_scale = int(tile_alpha_image.size[0])
            artwork_image = artwork_image.resize((x_scale, y_scale), Image.LANCZOS)
        cropped_artwork = artwork_image.crop((
            (1 + landscape_x_offset) * artwork_image.size[0]/2 - tile_alpha_image.size[0]/2,
            (1 + landscape_y_offset) * artwork_image.size[1]/2 - tile_alpha_image.size[1]/2,
            (1 + landscape_x_offset) * artwork_image.size[0]/2 + tile_alpha_image.size[0]/2,
            (1 + landscape_y_offset) * artwork_image.size[1]/2 + tile_alpha_image.size[1]/2,
        ))
        image.paste(cropped_artwork, tile_coords, tile_alpha_image)

        # Same thing with the logo
        # Crop image 
        bbox = logo_image.getbbox()
        logo_image = logo_image.crop(bbox)
        logo_image = remove_transparent_pixels(logo_image)
        logo_image_ratio = (logo_image.size[0] / logo_image.size[1])
        tile_alpha_image_ratio = (tile_alpha_image.size[0] / tile_alpha_image.size[1])
        if logo_image_ratio > 1 :
            x_scale = int(tile_alpha_image.size[0] * 0.7) # = x_logo / x_logo * x_tile 
            y_scale = int(tile_alpha_image.size[0] / logo_image_ratio * 0.7)
            logo_image = logo_image.resize((x_scale, y_scale), Image.LANCZOS)
        else:
            x_scale = int(tile_alpha_image.size[0] * logo_image_ratio * 0.4) # = x_logo / x_logo * x_tile 
            y_scale = int(tile_alpha_image.size[0] * 0.4)
            logging.info(f"YOUHOU {day}: {x_scale=}, {y_scale=}")
            logo_image = logo_image.resize((x_scale, y_scale), Image.LANCZOS)
        cropped_logo = logo_image.crop((
            logo_image.size[0]/2 - tile_alpha_image.size[0]/2,
            logo_image.size[1]/2 - tile_alpha_image.size[1]/2,
            logo_image.size[0]/2 + tile_alpha_image.size[0]/2,
            logo_image.size[1]/2 + tile_alpha_image.size[1]/2,
        ))
        # Logo color treatment
        r,g,b,a = cropped_logo.split()
        if logo_color == "white":
            r = r.point(lambda i: 255)
            g = g.point(lambda i: 255)
            b = b.point(lambda i: 255)
        if logo_color == "black":
            r = r.point(lambda i: 0)
            g = g.point(lambda i: 0)
            b = b.point(lambda i: 0)
        cropped_logo = Image.merge("RGBA", (r, g, b, a))

        logo_contour = cropped_logo
        logo_contour = logo_contour.filter(ImageFilter.GaussianBlur(5))
        # Change background color
        r,g,b,a = logo_contour.split()
        r = r.point(lambda i: 0 if background_logo_color == "black" else 255)
        g = g.point(lambda i: 0 if background_logo_color == "black" else 255)
        b = b.point(lambda i: 0 if background_logo_color == "black" else 255)
        a = a.point(lambda i: i*1.5 if background_logo_color == "black" else i)
        logo_contour = Image.merge("RGBA", (r, g, b, a))
        logo_y_offset = 0
        if landscape_logo_slot == "top":
            logo_y_offset = -1 * int(tile_alpha_image.size[1]/3.5) + 10
        elif landscape_logo_slot == "bottom":
            logo_y_offset = int(tile_alpha_image.size[1]/3.5) - 10
        logo_x_coord = tile_coords[0]
        logo_y_coord = tile_coords[1] + logo_y_offset
        image.alpha_composite(logo_contour, (logo_x_coord + 1, logo_y_coord + 1))
        image.alpha_composite(cropped_logo, (logo_x_coord, logo_y_coord))

        # Add the contour
        image.paste(tile_contour_image, tile_coords, tile_contour_image)
    
    def add_off_tile(image, day):
        # Compute coords of the time
        monday_offset = 135
        two_days_diff = 251
        offset_offtile = 556
        horizontal_coord = monday_offset + days_of_the_week.index(day) * two_days_diff
        vertical_coord = offset_offtile
        tile_coords = (horizontal_coord, vertical_coord)
        offtile_image = Image.open(os.path.join(layout_dir, "offtile_large.png"))
        r,g,b,a = offtile_image.split()
        r = r.point(lambda i: i*1.2)
        g = g.point(lambda i: i*1.2)
        b = b.point(lambda i: i*1.2)
        a = a.point(lambda i: 200*i if i > 0 else 0)
        offtile_image = Image.merge("RGBA", (r,g,b,a))
        image.alpha_composite(offtile_image, tile_coords)
        
    base_template_dir = os.path.join(os.getcwd(), "templates")
    games_dir = os.path.join(os.getcwd(), "games")
    layout_dir = os.path.join(base_template_dir, "layouts", config["layout"])
    fonts_dir = os.path.join(base_template_dir, "fonts")
    target_dir = os.path.join(os.getcwd(), "output", f"W{config['week']}")
    if not os.path.exists(target_dir):
        os.mkdir(target_dir)

    landscape_width = 1920
    landscape_height = 1080
    landscape_size = (landscape_width, landscape_height)
    landscape_image = Image.new("RGBA", landscape_size)

    # Copy the template base into the canvas
    with Image.open(os.path.join(layout_dir, "landscape_base.png")) as im:
        landscape_image.paste(im)

    # Compute and write date
    week_number = config["week"]
    date_string = f"2023-W{week_number}"
    first_date = datetime.datetime.strptime(date_string + "-1", "%Y-W%W-%w")
    last_date = first_date + datetime.timedelta(days=6)
    first_day = first_date.day
    last_day = last_date.day
    set_planning_title_text(landscape_image)

    for day in days_of_the_week:
        if day in config:
            day_config = config[day]
            add_game_tile(landscape_image, day, day_config)
        else:
            add_off_tile(landscape_image, day)

    output_path = os.path.join(target_dir, f"planning_W{week_number}_landscape.png")
    landscape_image.save(output_path)
    logging.info(f"Generated \"{output_path}\".")

def generate_planning_portrait(config):

    opacity_off = 0.5

    def set_planning_title_text(image):
        # Setup the text
        font_size = 45
        text_image = Image.new("RGBA", (2000, 1000), (0,0,0,0))
        draw = ImageDraw.Draw(text_image)
        draw.ink = "black"
        draw.fill = True

        color_scheme = theme_colors[config["layout"]]
        draw_text_in_font(draw, (100, 100), border = 2, content = [
            ("Programme du ", color_scheme["light"], os.path.join(fonts_dir, "Rodin Pro DB.otf"), font_size),
            (f"{first_day:02}/{first_date.month:02} ", color_scheme["strong"], os.path.join(fonts_dir, "Rodin Pro UB.otf"), int(font_size * 7/6)),
            # (f"{months[first_date.month]} ", color_scheme["strong"], os.path.join(fonts_dir, "Rodin Pro DB.otf"), int(font_size * 7/6)), # Additional line 
            ("au ", color_scheme["light"], os.path.join(fonts_dir, "Rodin Pro DB.otf"), font_size),
            (f"{last_day:02}/{last_date.month:02}", color_scheme["strong"], os.path.join(fonts_dir, "Rodin Pro UB.otf"), int(font_size * 7/6)),
            # (f"{months[last_date.month]}", color_scheme["strong"], os.path.join(fonts_dir, "Rodin Pro DB.otf"), int(font_size * 7/6)),
        ])

        # Crop image 
        bbox = text_image.getbbox()
        text_image = text_image.crop(bbox)
        (w, _) = text_image.size
        image.paste(text_image, (int(portrait_width/2 - w/2), 190), text_image)

    def add_game_tile(vertical_offset, image, day, day_config):
        # Extract values from day_config
        # Try to extract default values
        time_slot = check_if_else_default(day_config, "type", None)
        game_name = check_if_else_default(day_config, "game", None)
        game_config = ultra_default_config
        if os.path.exists(os.path.join(games_dir, game_name, "settings.toml")):
            with open(os.path.join(games_dir, game_name, "settings.toml"), "r") as f:
                loaded_config = toml.loads(f.read())
            for key, value in loaded_config.items():
                game_config[key] = value

        portrait_logo_slot = check_if_else_default(day_config, "portrait_logo_slot", game_config["portrait_logo_slot"])
        background_logo_color = check_if_else_default(day_config, "background_logo_color", game_config["background_logo_color"])
        portrait_x_offset = check_if_else_default(day_config, "portrait_x_offset", game_config["portrait_x_offset"])
        portrait_y_offset = check_if_else_default(day_config, "portrait_y_offset", game_config["portrait_y_offset"])
        logo_color = check_if_else_default(day_config, "logo_color", game_config["logo_color"])
        
        # Compute coords of the time
        vertical_online_size = 230
        horizontal_coord = 300
        vertical_coord = vertical_offset
        tile_coords = (horizontal_coord, vertical_coord)

        # Retrieve all data from the game folder
        artwork_image = None
        logo_image = None
        for extension in ["jpeg", "jpg", "png"]:
            if os.path.exists(os.path.join(games_dir, game_name, f"artwork.{extension}")):
                artwork_image = Image.open(os.path.join(games_dir, game_name, f"artwork.{extension}"))
                break
        if artwork_image is None:
            logging.error(f"Artwork image for game {game_name} is not found.")
            return

        for extension in ["png"]:
            if os.path.exists(os.path.join(games_dir, game_name, f"logo.{extension}")):
                logo_image = Image.open(os.path.join(games_dir, game_name, f"logo.{extension}"))
                logo_image = format_to_four_unpack(logo_image)
                break
        if logo_image is None:
            logging.warning(f"Logo image for game {game_name} is not found. Might be intentional.")

        tile_background_image = Image.open(os.path.join(base_template_dir, "portrait_background_tile.png"))
        tile_alpha_image = Image.open(os.path.join(base_template_dir, "portrait_alpha_tile.png"))
        tile_contour_image = Image.open(os.path.join(base_template_dir, "portrait_tile.png"))

        # First, paste the background tile as transparent
        r,g,b,a = tile_background_image.split()
        a = a.point(lambda i : 0.3 * i)
        tile_background_image = Image.merge("RGBA", (r,g,b,a))
        image.paste(tile_background_image, tile_coords, tile_background_image)
        # First, paste the tile_alpha
        image.paste(tile_alpha_image, tile_coords, tile_alpha_image)

        # Multiply the artwork with the alpha
        artwork_image_ratio = (artwork_image.size[0] / artwork_image.size[1])
        tile_alpha_image_ratio = (tile_alpha_image.size[0] / tile_alpha_image.size[1])
        if artwork_image_ratio > tile_alpha_image_ratio :
            x_scale = int(tile_alpha_image.size[1] * artwork_image_ratio)
            y_scale = int(tile_alpha_image.size[1])
            artwork_image = artwork_image.resize((x_scale, y_scale), Image.LANCZOS)
        else:
            x_scale = int(tile_alpha_image.size[0] * artwork_image_ratio)
            y_scale = int(tile_alpha_image.size[0])
            artwork_image = artwork_image.resize((x_scale, y_scale), Image.LANCZOS)
        cropped_artwork = artwork_image.crop((
            int((1 + portrait_x_offset) * artwork_image.size[0]/2.0 - tile_alpha_image.size[0]/2.0),
            int((1 + portrait_y_offset) * artwork_image.size[1]/2.0 - tile_alpha_image.size[1]/2.0),
            int((1 + portrait_x_offset) * artwork_image.size[0]/2.0 + tile_alpha_image.size[0]/2.0),
            int((1 + portrait_y_offset) * artwork_image.size[1]/2.0 + tile_alpha_image.size[1]/2.0),
        ))
        image.paste(cropped_artwork, tile_coords, tile_alpha_image)

        # Same thing with the logo
        # Crop image 
        bbox = logo_image.getbbox()
        logo_image = logo_image.crop(bbox)
        logo_image = remove_transparent_pixels(logo_image)
        logo_image_ratio = (logo_image.size[0] / logo_image.size[1])
        tile_alpha_image_ratio = (tile_alpha_image.size[0] / tile_alpha_image.size[1])
        if logo_image_ratio < 2 : # X > 2*Y
            y_scale = int(tile_alpha_image.size[1] * 0.7)
            x_scale = int(tile_alpha_image.size[1] * 0.7 * logo_image_ratio)
            logo_image = logo_image.resize((x_scale, y_scale), Image.LANCZOS)
        else:
            x_scale = int(tile_alpha_image.size[0] * 0.35) # = x_logo / x_logo * x_tile 
            y_scale = int(tile_alpha_image.size[0] * 0.35 / logo_image_ratio)
            logo_image = logo_image.resize((x_scale, y_scale), Image.LANCZOS)
        cropped_logo = logo_image.crop((
            logo_image.size[0]/2 - tile_alpha_image.size[0]/2,
            logo_image.size[1]/2 - tile_alpha_image.size[1]/2,
            logo_image.size[0]/2 + tile_alpha_image.size[0]/2,
            logo_image.size[1]/2 + tile_alpha_image.size[1]/2,
        ))
        # Logo color treatment
        r,g,b,a = cropped_logo.split()
        if logo_color == "white":
            r = r.point(lambda i: 255)
            g = g.point(lambda i: 255)
            b = b.point(lambda i: 255)
        if logo_color == "black":
            r = r.point(lambda i: 0)
            g = g.point(lambda i: 0)
            b = b.point(lambda i: 0)
        cropped_logo = Image.merge("RGBA", (r, g, b, a))

        logo_contour = cropped_logo
        logo_contour = logo_contour.filter(ImageFilter.GaussianBlur(5))
        # Change background color
        r,g,b,a = logo_contour.split()
        r = r.point(lambda i: 0 if background_logo_color == "black" else 255)
        g = g.point(lambda i: 0 if background_logo_color == "black" else 255)
        b = b.point(lambda i: 0 if background_logo_color == "black" else 255)
        a = a.point(lambda i: i*1.5)
        logo_contour = Image.merge("RGBA", (r, g, b, a))
        logo_x_offset = 0
        logo_y_offset = 0
        if portrait_logo_slot == "left":
            logo_x_offset = -1 * int(tile_alpha_image.size[0]/2.0 - logo_image.size[0]/2.0 - 50)
            logo_y_offset = 10 # put it lower to avoid the future contour
        elif portrait_logo_slot == "right":
            logo_x_offset = int(tile_alpha_image.size[0]/2.0 - logo_image.size[0]/2.0) - 50
        logo_x_coord = tile_coords[0] + logo_x_offset
        logo_y_coord = tile_coords[1] + logo_y_offset
        image.alpha_composite(logo_contour, (logo_x_coord + 1, logo_y_coord + 1))
        image.alpha_composite(cropped_logo, (logo_x_coord, logo_y_coord))

        # Paste the contour
        image.paste(tile_contour_image, tile_coords, tile_contour_image)

        # Write the hour in the hour box in the countour
        # Create text
        font_size = 25
        text_image = Image.new("RGBA", (2000, 1000), (0,0,0,0))
        draw = ImageDraw.Draw(text_image)
        draw.ink = "black"
        draw.fill = True

        color_scheme = theme_colors[config["layout"]]
        draw_text_in_font(draw, (100, 100), border = 1, content = [
            (f"{time_slot} ", color_scheme["light"], os.path.join(fonts_dir, "Rodin Pro UB.otf"), font_size),
        ])
        # Crop image 
        bbox = text_image.getbbox()
        text_image = text_image.crop(bbox)
        (w, _) = text_image.size
        
        # Compute coordinates and paste image
        horizontal_coord = int(horizontal_coord + 55 - text_image.size[0]/2.0)
        vertical_coord = vertical_offset + 20
        tile_coords = (horizontal_coord, vertical_coord)
        image.paste(text_image, tile_coords, text_image)

        return vertical_offset + vertical_online_size

    def add_off_tile(vertical_offset, image, day):
        # Compute coords of the time
        vertical_offline_size = 70
        horizontal_coord = 300
        vertical_coord = vertical_offset
        tile_coords = (horizontal_coord, vertical_coord)

        # First, paste the background tile as transparent
        tile_background_image = Image.open(os.path.join(base_template_dir, "portrait_background_off_tile.png"))
        r,g,b,a = tile_background_image.split()
        a = a.point(lambda i : opacity_off * i)
        tile_background_image = Image.merge("RGBA", (r,g,b,a))
        image.alpha_composite(tile_background_image, tile_coords)

        # Create "offline" text
        font_size = 30
        text_image = Image.new("RGBA", (2000, 1000), (0,0,0,0))
        draw = ImageDraw.Draw(text_image)
        draw.ink = "black"
        draw.fill = True

        color_scheme = theme_colors[config["layout"]]
        draw_text_in_font(draw, (100, 100), border = 2, content = [
            (f"OFF", theme_colors["grey"], os.path.join(fonts_dir, "Rodin Pro DB.otf"), font_size),
        ])
        # Crop image 
        bbox = text_image.getbbox()
        text_image = text_image.crop(bbox)
        (w, _) = text_image.size

        r,g,b,a = text_image.split()
        a = a.point(lambda i : int(0.7*i))
        text_image = Image.merge("RGBA", (r,g,b,a))
        
        # Compute coordinates and paste image
        horizontal_coord = int(horizontal_coord + tile_background_image.size[0]/2.0 - text_image.size[0]/2.0)
        vertical_coord = int(vertical_coord + tile_background_image.size[1]/2.0 - text_image.size[1]/2.0)
        tile_coords = (horizontal_coord, vertical_coord)
        image.paste(text_image, tile_coords, text_image)
        return vertical_offset + vertical_offline_size
 
    def apply_tile_date(vertical_offset, image, day, on_bool):
        # Create text
        font_size = 40 if on_bool else 30
        text_image = Image.new("RGBA", (2000, 1000), (0,0,0,0))
        draw = ImageDraw.Draw(text_image)
        draw.ink = "black"
        draw.fill = True

        color_scheme = theme_colors[config["layout"]]
        draw_text_in_font(draw, (100, 100), border = 3 if on_bool else 2, content = [
            (f"{day.upper()[0:3]} ", color_scheme["light"], os.path.join(fonts_dir, "Rodin Pro DB.otf"), font_size),
            (f"{first_day + days_of_the_week.index(day)} ", color_scheme["light"], os.path.join(fonts_dir, "Rodin Pro UB.otf"), font_size), # Additional line 
        ])
        # Crop image 
        bbox = text_image.getbbox()
        text_image = text_image.crop(bbox)
        (w, _) = text_image.size

        r,g,b,a = text_image.split()
        a = a.point(lambda i : i if on_bool else int(opacity_off * i))
        text_image = Image.merge("RGBA", (r,g,b,a))
        
        # Compute coordinates and paste image
        horizontal_coord = int(275 - text_image.size[0])
        vertical_coord = vertical_offset + 90 if on_bool else vertical_offset + 15
        tile_coords = (horizontal_coord, vertical_coord)
        image.alpha_composite(text_image, tile_coords)


    base_template_dir = os.path.join(os.getcwd(), "templates")
    games_dir = os.path.join(os.getcwd(), "games")
    layout_dir = os.path.join(base_template_dir, "layouts", config["layout"])
    fonts_dir = os.path.join(base_template_dir, "fonts")
    target_dir = os.path.join(os.getcwd(), "output", f"W{config['week']}")
    if not os.path.exists(target_dir):
        os.mkdir(target_dir)

    portrait_width = 1080
    portrait_height = 1920
    portrait_size = (portrait_width, portrait_height)
    portrait_image = Image.new("RGBA", portrait_size)

    # Copy the template base into the canvas
    with Image.open(os.path.join(layout_dir, "portrait_base.png")) as im:
        portrait_image.paste(im)
    
    # Compute and write date
    week_number = config["week"]
    date_string = f"2023-W{week_number}"
    first_date = datetime.datetime.strptime(date_string + "-1", "%Y-W%W-%w")
    last_date = first_date + datetime.timedelta(days=6)
    first_day = first_date.day
    last_day = last_date.day
    set_planning_title_text(portrait_image)

    # Set days on the planning
    tile_vertical_pos = 280
    for day in days_of_the_week:
        apply_tile_date(tile_vertical_pos, portrait_image, day, day in config)
        if day in config:
            day_config = config[day]
            tile_vertical_pos = add_game_tile(tile_vertical_pos, portrait_image, day, day_config)
        else:
            tile_vertical_pos = add_off_tile(tile_vertical_pos, portrait_image, day)

    output_path = os.path.join(target_dir, f"planning_W{week_number}_portrait.png")
    portrait_image.save(output_path)
    logging.info(f"Generated \"{output_path}\".")

def generate_daily_portrait(day, config):
    
    def set_planning_title_text(image):
        # Setup the text
        font_size = 90
        text_image = Image.new("RGBA", (2000, 1000), (0,0,0,0))
        draw = ImageDraw.Draw(text_image)
        draw.ink = "black"
        draw.fill = True

        color_scheme = theme_colors[config["layout"]]
        draw_text_in_font(draw, (100, 100), border = 4, content = [
            (f"{day.upper()}", color_scheme["strong"], os.path.join(fonts_dir, "Rodin Pro DB.otf"), font_size),
        ])

        # Crop image 
        bbox = text_image.getbbox()
        text_image = text_image.crop(bbox)
        (w, _) = text_image.size
        image.paste(text_image, (int(portrait_width/2 - w/2), 260), text_image)

    def add_game_tile(image, day, day_config):
        # Extract values from day_config
        # Try to extract default values
        time_slot = check_if_else_default(day_config, "type", None)
        game_name = check_if_else_default(day_config, "game", None)
        game_config = ultra_default_config
        if os.path.exists(os.path.join(games_dir, game_name, "settings.toml")):
            with open(os.path.join(games_dir, game_name, "settings.toml"), "r") as f:
                loaded_config = toml.loads(f.read())
            for key, value in loaded_config.items():
                game_config[key] = value

        landscape_logo_slot = check_if_else_default(day_config, "landscape_logo_slot", game_config["landscape_logo_slot"])
        background_logo_color = check_if_else_default(day_config, "background_logo_color", game_config["background_logo_color"])
        daily_x_offset = check_if_else_default(day_config, "daily_x_offset", game_config["daily_x_offset"])
        daily_y_offset = check_if_else_default(day_config, "daily_y_offset", game_config["daily_y_offset"])
        logo_color = check_if_else_default(day_config, "logo_color", game_config["logo_color"])


        # Retrieve all data from the game folder
        artwork_image = None
        logo_image = None
        for extension in ["jpeg", "jpg", "png"]:
            if os.path.exists(os.path.join(games_dir, game_name, f"artwork.{extension}")):
                artwork_image = Image.open(os.path.join(games_dir, game_name, f"artwork.{extension}"))
                break
        if artwork_image is None:
            logging.error(f"Artwork image for game {game_name} is not found.")
            return

        for extension in ["png"]:
            if os.path.exists(os.path.join(games_dir, game_name, f"logo.{extension}")):
                logo_image = Image.open(os.path.join(games_dir, game_name, f"logo.{extension}"))
                logo_image = format_to_four_unpack(logo_image)
                break
        if logo_image is None:
            logging.warning(f"Logo image for game {game_name} is not found. Might be intentional.")

        tile_background_image = Image.open(os.path.join(base_template_dir, "daily_background_tile.png"))
        tile_alpha_image = Image.open(os.path.join(base_template_dir, "daily_alpha_tile.png"))
        tile_contour_image = Image.open(os.path.join(base_template_dir, "daily_tile.png"))

        # First, paste the background tile as transparent
        r,g,b,a = tile_background_image.split()
        a = a.point(lambda i : 0.3 * i)
        tile_background_image = Image.merge("RGBA", (r,g,b,a))

        # Multiply the artwork with the alpha
        artwork_image = artwork_image.crop(artwork_image.getbbox())
        tile_alpha_image = tile_alpha_image.crop(tile_alpha_image.getbbox())

        # Multiply the artwork with the alpha
        artwork_image_ratio = (artwork_image.size[0] / artwork_image.size[1])
        tile_alpha_image_ratio = (tile_alpha_image.size[0] / tile_alpha_image.size[1])
        if artwork_image_ratio > tile_alpha_image_ratio :
            x_scale = int(tile_alpha_image.size[1] * artwork_image_ratio)
            y_scale = int(tile_alpha_image.size[1])
            artwork_image = artwork_image.resize((x_scale, y_scale), Image.LANCZOS)
        else:
            x_scale = int(tile_alpha_image.size[0] * artwork_image_ratio)
            y_scale = int(tile_alpha_image.size[0])
            artwork_image = artwork_image.resize((x_scale, y_scale), Image.LANCZOS)
        cropped_artwork = artwork_image.crop((
            (1 + daily_x_offset) * artwork_image.size[0]/2 - tile_alpha_image.size[0]/2,
            (1 + daily_y_offset) * artwork_image.size[1]/2 - tile_alpha_image.size[1]/2,
            (1 + daily_x_offset) * artwork_image.size[0]/2 + tile_alpha_image.size[0]/2,
            (1 + daily_y_offset) * artwork_image.size[1]/2 + tile_alpha_image.size[1]/2,
        ))

        # Same thing with the logo
        # Crop image 
        bbox = logo_image.getbbox()
        logo_image = logo_image.crop(bbox)
        logo_image_ratio = (logo_image.size[0] / logo_image.size[1])
        tile_alpha_image_ratio = (tile_alpha_image.size[0] / tile_alpha_image.size[1])
        if logo_image_ratio > tile_alpha_image_ratio :
            x_scale = int(tile_alpha_image.size[0] * 0.8) # = x_logo / x_logo * x_tile 
            y_scale = int(logo_image.size[1] * tile_alpha_image.size[0] / logo_image.size[0] * 0.8)
            logo_image = logo_image.resize((x_scale, y_scale), Image.LANCZOS)
        else:
            x_scale = int(tile_alpha_image.size[1] * 0.8) # = x_logo / x_logo * x_tile 
            y_scale = int(logo_image.size[0] * tile_alpha_image.size[1] / logo_image.size[1] * 0.8)
            logo_image = logo_image.resize((x_scale, y_scale), Image.LANCZOS)
        cropped_logo = logo_image.crop((
            logo_image.size[0]/2 - tile_alpha_image.size[0]/2,
            logo_image.size[1]/2 - tile_alpha_image.size[1]/2,
            logo_image.size[0]/2 + tile_alpha_image.size[0]/2,
            logo_image.size[1]/2 + tile_alpha_image.size[1]/2,
        ))
        # Logo color treatment
        r,g,b,a = cropped_logo.split()
        if logo_color == "white":
            r = r.point(lambda i: 255)
            g = g.point(lambda i: 255)
            b = b.point(lambda i: 255)
        if logo_color == "black":
            r = r.point(lambda i: 0)
            g = g.point(lambda i: 0)
            b = b.point(lambda i: 0)
        cropped_logo = Image.merge("RGBA", (r, g, b, a))

        logo_contour = cropped_logo
        logo_contour = logo_contour.filter(ImageFilter.GaussianBlur(5))
        # Change background color
        r,g,b,a = logo_contour.split()
        r = r.point(lambda i: 0 if background_logo_color == "black" else 255)
        g = g.point(lambda i: 0 if background_logo_color == "black" else 255)
        b = b.point(lambda i: 0 if background_logo_color == "black" else 255)
        a = a.point(lambda i: i*1.5)
        logo_contour = Image.merge("RGBA", (r, g, b, a))

        bbox = tile_contour_image.getbbox()
        tile_contour_image = tile_contour_image.crop(bbox)

        # Compute coords
        horizontal_coord = int(1080/2.0 - tile_background_image.size[0]/2.0)
        vertical_coord = 380
        tile_coords = (horizontal_coord, vertical_coord)
        
        # Paste the tiles
        image.paste(tile_background_image, tile_coords, tile_background_image)
        tile_coords = (horizontal_coord + 10, vertical_coord + 10)
        image.paste(cropped_artwork, tile_coords, tile_alpha_image)
        # image.paste(tile_alpha_image, tile_coords, tile_alpha_image)
        logo_x_offset = 0
        logo_y_offset = 0
        if landscape_logo_slot == "top":
            logo_y_offset = -1 * int(tile_alpha_image.size[1]/3)
        elif landscape_logo_slot == "bottom":
            logo_y_offset = int(tile_alpha_image.size[1]/3)
        logo_x_coord = tile_coords[0] + logo_x_offset
        logo_y_coord = tile_coords[1] + logo_y_offset
        image.paste(logo_contour, (logo_x_coord + 1, logo_y_coord + 1), logo_contour)
        image.paste(cropped_logo, (logo_x_coord, logo_y_coord), cropped_logo)

        # Paste the contour
        image.paste(tile_contour_image, tile_coords, tile_contour_image)

        # Write the hour in the hour box in the countour
        # Create text
        font_size = 40
        text_image = Image.new("RGBA", (2000, 1000), (0,0,0,0))
        draw = ImageDraw.Draw(text_image)
        draw.ink = "black"
        draw.fill = True

        color_scheme = theme_colors[config["layout"]]
        draw_text_in_font(draw, (100, 100), border = 1, content = [
            (f"{time_slot} ", color_scheme["light"], os.path.join(fonts_dir, "Rodin Pro UB.otf"), font_size),
        ])
        # Crop image 
        bbox = text_image.getbbox()
        text_image = text_image.crop(bbox)
        (w, _) = text_image.size
        
        # Compute coordinates and paste image
        horizontal_coord = int(horizontal_coord + 70 - text_image.size[0]/2.0)
        tile_coords = (horizontal_coord, vertical_coord + 20)
        image.paste(text_image, tile_coords, text_image)

    base_template_dir = os.path.join(os.getcwd(), "templates")
    games_dir = os.path.join(os.getcwd(), "games")
    layout_dir = os.path.join(base_template_dir, "layouts", config["layout"])
    fonts_dir = os.path.join(base_template_dir, "fonts")
    target_dir = os.path.join(os.getcwd(), "output", f"W{config['week']}")
    if not os.path.exists(target_dir):
        os.mkdir(target_dir)

    portrait_width = 1080
    portrait_height = 1920
    portrait_size = (portrait_width, portrait_height)
    portrait_image = Image.new("RGBA", portrait_size)

    # Copy the template base into the canvas
    with Image.open(os.path.join(layout_dir, "daily_base.png")) as im:
        portrait_image.paste(im)
    
    # Compute and write date
    week_number = config["week"]
    date_string = f"2023-W{week_number}"
    first_date = datetime.datetime.strptime(date_string + "-1", "%Y-W%W-%w")
    last_date = first_date + datetime.timedelta(days=6)
    first_day = first_date.day
    last_day = last_date.day
    set_planning_title_text(portrait_image)

    # Set days on the planning
    add_game_tile(portrait_image, day, config[day])

    output_path = os.path.join(target_dir, f"planning_W{week_number}_{days_of_the_week.index(day)}.png")
    portrait_image.save(output_path)
    logging.info(f"Generated \"{output_path}\".")

def generate_daily_tweet(day, day_config):
    pass

def main():
    # Get the source directory of the project
    file = sys.argv[0]
    old_cwd = os.getcwd()
    new_cwd = os.path.abspath(file).replace("src/main.py", "")
    os.chdir(new_cwd)

    
    args = arguments()
    filename = args.filename
    if not os.path.exists(filename):
        logging.error(f"File {filename} does not exist. Exiting.")
        exit(1)
    config = get_config(filename)

    # Configure logger
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(CustomFormatter())
    logger.addHandler(ch)
    
    output_folder = os.path.join(os.getcwd(), "output", f"W{config['week']}")
    if not os.path.exists(output_folder):
        os.mkdir(output_folder)
        logging.info(f"Created folder {output_folder}.")

    try:
        generate_planning_landscape(config) 
    except Exception as e:
        error_type = e.__class__.__name__
        logging.error(f"{error_type:20} -> {e}")
            
    # try:
    #     generate_planning_portrait(config)
    # except Exception as e:
    #     error_type = e.__class__.__name__
    #     logging.error(f"{error_type:20} -> {e}")

    # for day in days_of_the_week:
    #     if day in config:
    #         generate_daily_tweet(day, config)
    #         generate_daily_portrait(day, config)
            
    os.chdir(old_cwd)
    exit()

if __name__ == "__main__":
    main()
