# Pilanning, PILLOW-based planning generator

A planning generator I made for my streams, using [PILLOW](https://pillow.readthedocs.io/en/stable/). Full of bugs, but stable enough so I can use it.

## Requirements
Python 3.8+ is required.

## Contribute
Please create any pull request for contribution, as I will read them when I have the time.

## How to generate a planning?
First, install the requirements: `pip install -r requirements.txt`
Then, define games in the `games` folder, with a folder per game (`Witcher 3`for instance), and inside of this folder will be needed the following elements:

* an artwork image, named `artwork.jpg` (png and jpeg file extensions also work)
* a logo image, named `logo.png`.
* You can also put a `settings.toml` file to fine-tune the settings. See following chapter to see the settings.

Once you have set-up those files, you can define a TOML file in the `var` folder (or anywhere, but that's where I store them), with the following entries:

* A `layout`variable, that can be bridge, cherry, pond or winter. This is the "theme" for the template.
* A `week` number, this will be used to compute the week days on top of the calendar.
* For each day of the week, define a `[day]` where you will put:
    * a `type` entry, that can be `14h` or `20h`. This will determine the emplacement of the planning in the day column.
	* a `game` entry, refering the game folder made before
	* optionally, settings (see next chapter).
 
Once the TOML file is defined, you can run the following command: `python src/main.py var/<your_file>.TOML`.

## Settings

You can set the following settings in either your var TOML file, or in your game folder TOML file:

* `logo_color` to override the logo color (Between default, white, black)
* `background_logo_color` to set the logo background color (between white, black)
* `landscape_logo_slot` to set the logo emplacement in landscape setup (between bottom, middle, top)
* `landscape_y_offset` to move the artwork.jpg around in landscape setup (-0.5 to 0.5, careful to result)
* `landscape_x_offset` to move the artwork.jpg around in landscape setup (-0.5 to 0.5, careful to result)
* `portrait_logo_slot` to set the logo emplacement in portrait setup (left, middle, right)
* `portrait_x_offset` to move the artwork.jpg around in portrait setup (-0.5 to 0.5, careful to result)
* `portrait_y_offset` to move the artwork.jpg around in portrait setup (-0.5 to 0.5, careful to result)

If the same setting is set in both files, the var TOML file will prevail upon the game settings.toml.